package com.bgiaa.flash_card_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.bgiaa.flash_card_android.api.AnswersData;
import com.bgiaa.flash_card_android.api.PokerAPI;
import com.bgiaa.flash_card_android.api.QuestionWrapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CardListActivity extends AppCompatActivity {


    private final List<FlashCard> cards = new ArrayList<FlashCard>();

    private CardAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_list);

        //REQUETE HTTP
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://gryt.tech:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Generation API
        PokerAPI api = retrofit.create(PokerAPI.class);

        //Creation de la requete
        Call<List<QuestionWrapper>> call = api.getQuestions();

        //Execution de la requete en asynchrone
        call.enqueue(new Callback<List<QuestionWrapper>>() {
            @Override
            public void onResponse(Call<List<QuestionWrapper>> call, Response<List<QuestionWrapper>> response) {
                for (QuestionWrapper q : response.body()) {
                    ArrayList<FlashCardAnswer> fca = new ArrayList<FlashCardAnswer>();
                    for (AnswersData a: q.answers) {
                        fca.add(new FlashCardAnswer(a.sentence, a.is_right));
                    }

                    //cards.add(new FlashCard(R.drawable.aa, "czencjz", q.difficulty, fca));
                    cards.add(new FlashCard(JSONHandler.mapsImagetoDrawable(q.image), "czencjz", q.difficulty, fca));
                }
                Log.i("cardListActivity", "onResponse: " + cards.get(0).getDifficulty());
                linkAdapter();
            }

            @Override
            public void onFailure(Call<List<QuestionWrapper>> call, Throwable t) {
                Log.i("cardListActivity", "onFailure: " + t);

            }
        });








    }

    public void linkAdapter(){
        adapter = new CardAdapter(cards);
        RecyclerView recyclerView = findViewById(R.id.RecyclerViewId);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }



    
}