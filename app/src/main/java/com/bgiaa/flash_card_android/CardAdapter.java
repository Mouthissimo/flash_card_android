package com.bgiaa.flash_card_android;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder>{

    private List<FlashCard> cards;

    public CardAdapter(List<FlashCard> cards) {
        this.cards = cards;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final ImageView handPic;
        final TextView difficulty;
        public ViewHolder(@NonNull View itemView) { super(itemView);
            handPic = itemView.findViewById(R.id.handPicture);
            difficulty = itemView.findViewById(R.id.Difficulty);
        }

    }

    @NonNull
    @Override
    public CardAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // creating the backbone which will holds our cards
        View viewItem = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_layout, parent, false);
        return new ViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(@NonNull CardAdapter.ViewHolder holder, int position) {
        // Getting data from position
        FlashCard card = cards.get(position);

        // Using our card data to fill our view
        holder.handPic.setImageResource(card.getCombiImage());
        holder.difficulty.setText(card.getDifficulty());
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }
}
