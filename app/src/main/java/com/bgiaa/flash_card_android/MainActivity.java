package com.bgiaa.flash_card_android;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bgiaa.flash_card_android.api.AnswersData;
import com.bgiaa.flash_card_android.api.PokerAPI;
import com.bgiaa.flash_card_android.api.QuestionWrapper;
import com.bgiaa.flash_card_android.api.QuestionWrapperDifficulty;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    private final ArrayList<FlashCard> cards = new ArrayList<FlashCard>();
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Handling clicks
        final Button aboutBtn = findViewById(R.id.aboutBtn);
        final Button listBtn = findViewById(R.id.listBtn);
        final Button playBtn = findViewById(R.id.playBtn);
        final ImageButton robtn = findViewById(R.id.imageBtnRob);


        aboutBtn.setOnClickListener(this);
        listBtn.setOnClickListener(this);
        playBtn.setOnClickListener(this);
        robtn.setOnClickListener(this);
        /////////
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.aboutBtn:
                navigaterino(AProposActivity.class);
                break;
            case R.id.listBtn:
                navigaterino(CardListActivity.class);
                break;
            case R.id.playBtn:
                View modal = modalOuDialog(v);
                handlerClickModal(modal);
                break;
            case R.id.imageBtnRob:
                navigaterino(robinScreenActivity.class);
        }

    }

    private void navigaterino(Class dest) {
        Intent intent = new Intent(this, dest);
        startActivity(intent);
    }

    private View modalOuDialog(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(v.getContext()).inflate(R.layout.item_modalerino, viewGroup, false);
        builder.setView(dialogView);
        alertDialog = builder.create();
        alertDialog.show();
        return dialogView;
    }

    private void handlerClickModal(View v)
    {
        final Button easyBtn = v.findViewById(R.id.ezBtn);
        final Button hardBtn = v.findViewById(R.id.hardBtn);

        easyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //its easy
                APICALL("easy");
                if (alertDialog != null) {alertDialog.dismiss();}
                //navigaterino(CardActivity.class);
            }
        });
        hardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //its hard
                APICALL("hard");
                if (alertDialog != null) {alertDialog.dismiss();}
                //navigaterino(CardActivity.class);
            }
        });
    }

    private void APICALL(String difficulty){


        //REQUETE HTTP
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://gryt.tech:8080/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Generation API
        PokerAPI api = retrofit.create(PokerAPI.class);

        //Creation de la requete
        Call<List<QuestionWrapperDifficulty>> call = api.getQuestionsbyDifficulty(difficulty);

        //Execution de la requete en asynchrone
        call.enqueue(new Callback<List<QuestionWrapperDifficulty>>() {
            @Override
            public void onResponse(Call<List<QuestionWrapperDifficulty>> call, Response<List<QuestionWrapperDifficulty>> response) {

                cards.clear();
                for (QuestionWrapperDifficulty q : response.body()) {
                    ArrayList<FlashCardAnswer> fca = new ArrayList<FlashCardAnswer>();
                    for (AnswersData a: q.answers) {
                        fca.add(new FlashCardAnswer(a.sentence, a.is_right));
                    }
                    //cards.add(new FlashCard(R.drawable.aa, "czencjz", q.difficulty, fca));
                        cards.add(new FlashCard(JSONHandler.mapsImagetoDrawable(q.image), "Quelle est la range minimale necessaire pour ouvrir cette main?", q.difficulty, fca));
                }
                //Log.i("MainActivity", "onResponse: " + cards.get(0).getCombiImage());
                DataIntent();
            }

            @Override
            public void onFailure(Call<List<QuestionWrapperDifficulty>> call, Throwable t) {

            }
        });
    }

    private void DataIntent(){
        Intent intent = new Intent(this, CardActivity.class);
        Collections.shuffle(cards);
        intent.putParcelableArrayListExtra("cards", cards);
        startActivityForResult(intent, 1);
    }

}
