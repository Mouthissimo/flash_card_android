package com.bgiaa.flash_card_android.api;

import java.util.List;

public class QuestionWrapper {
    public String image;
    public String difficulty;
    public List<AnswersData> answers;
}
